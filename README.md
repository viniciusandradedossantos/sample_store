# sample_store

Sample store project made to study Node.js, Express, MongoDB and Passport.js.

This is based but not limited to the lessons of the Zenva course "Server-Side Rendered Webapps with Node.js, Express and MongoDB". Most of what I used here I learned on Zenva courses and highly recomend it.
More references on the site https://academy.zenva.com/product/server-side-rendered-webapps-with-node-js-express-and-mongodb/

