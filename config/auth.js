const LocalStrategy = require('passport-local').Strategy;
const User = require('../models/User');
const bcrypt = require('bcryptjs');

module.exports = (passport) => {

    passport.serializeUser((user, next) => {
        next(null, user);
    });
    passport.deserializeUser((id, next) => {
        User.findById(id, (err, user) => {
            next(err, user);
        });
    });

    const localLogin = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, (req, email, password, next) => {

        if (email === '') {
            let e = new Error('Please enter the Email');
            e.body = req.body;
            return next(e);
        }
        if (password === '') {
            let e = new Error('Please enter the password');
            e.body = req.body;
            return next(e);
        }

        User.findOne({ email: email }, (err, user) => {
            if (err) {
                err.body = req.body;
                return next(err);
            }
            else if (!user) {
                let e = new Error('Email or password is not valid');
                e.body = req.body;
                return next(e);
            }
            if (!bcrypt.compareSync(password, user.password)) {
                let e = new Error('Email or password is not valid');
                e.body = req.body;
                return next(e);
            }
            else {
                return next(null, user);
            }
        });
    });
    passport.use('localLogin', localLogin);

    const localRegister = new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password',
        passReqToCallback: true
    }, (req, email, password, next) => {

        User.findOne({ email: email }, (err, data) => {
            if (err) {
                return next(err);
            }
            else if (data) {
                return next(new Error('Email already in use'));
            }
            else {
                const hashedPw = bcrypt.hashSync(password, 10);
                User.create({ email: email, password: hashedPw }, (err, data) => {
                    if (err) {
                        return next(err);
                    }
                    else {
                        return next(null, data);
                    }
                });
            }
        });
    });
    passport.use('localRegister', localRegister);
}

