const express = require('express');
const path = require('path');
const mongoose = require('mongoose');
const passport = require('passport');
const session = require('express-session');

const auth = require('./config/auth')(passport);
const home = require('./routes/home');
const register = require('./routes/register');
const login = require('./routes/login');
const account = require('./routes/account');
const admin = require('./routes/admin');

mongoose.connect('mongodb://localhost/samplestore', { useNewUrlParser: true }, (err, data) => {
    if (err) {
        console.log('DB connection fail ' + err.message);
    }
    else {
        console.log('DB connection succeded');
    }
});

const app = express();
app.use(session({
    secret: 'fsdgdfgeryert',
    resave: true,
    saveUninitialized: true
}));
app.use(passport.initialize());
app.use(passport.session());
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hjs');

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', home);
app.use('/register', register);
app.use('/login', login);
app.use('/account', account);
app.use('/admin', admin);

app.use((err, req, res, next) => {
    console.log('err: ' + err);
    res.render('home', {
        message: err,
        email: (err.body ? err.body.email : null),
        partials: {
            header: '../views/partials/header',
            footer: '../views/partials/footer',
            msg: '../views/partials/msg'
        }
    });
});


app.listen(5000);
console.log('http://localhost:5000/');