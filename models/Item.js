
const mongoose = require('mongoose');

const Item = new mongoose.Schema({
    name: { type: String, default: '', unique: true, trim: true },
    price: { type: Number, default: 0 },
    description: { type: String, default: '', trim: true },
    interested: { type: Array, default: [] },
    timestamp: { type: Date, default: Date.now }
});

Item.post('init', doc => {
    doc.hits = doc.interested ? doc.interested.length : 0;
});

module.exports = mongoose.model('Item', Item);