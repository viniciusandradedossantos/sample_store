const express = require('express');
const router = express.Router();
const Item = require('../models/Item');

function renderAccount(req, res, next, message) {
    if (req.user) {
        Item.find({ interested: { $ne: req.user._id } }, function (err, items) {
            if (err) {
                next(err);
            }
            else {
                Item.find({ interested: req.user._id }, function (err2, interestedItems) {
                    if (err2) {
                        next(err2);
                    }
                    else {
                        let total = 0;
                        interestedItems.forEach((item) => {
                            total += item.price;
                        });

                        const p = {
                            user: req.user,
                            message: message,
                            items: items,
                            interestedItems: interestedItems,
                            total: total,
                            partials: {
                                header: '../views/partials/header',
                                footer: '../views/partials/footer',
                                msg: '../views/partials/msg'
                            }
                        };

                        res.render('account', p);
                    }
                });
            }
        });
    }
    else {
        next(new Error('You are not logged in'));
    }
}

router.get('/', (req, res, next) => {
    renderAccount(req, res, next, null);
});


router.get('/logout', (req, res, next) => {
    req.logout();
    res.render('home', {
        message: 'Logged out', partials: {
            header: '../views/partials/header',
            footer: '../views/partials/footer',
            msg: '../views/partials/msg'
        }
    });
})

router.get('/additem/:itemid', (req, res, next) => {
    if (req.user) {
        Item.findById(req.params.itemid, function (err, item) {
            if (err)
                return next(err);

            if (item.interested.indexOf(req.user._id) == -1) {
                item.interested.push(req.user._id);
                item.save((err2, doc) => {
                    renderAccount(req, res, next, item.name + ' added');
                });
            }
            else {
                renderAccount(req, res, next, item.name + ' already whislisted');
            }
        });
    }
    else {
        next(new Error('You are not logged in'));
    }
})

router.get('/remitem/:itemid', (req, res, next) => {
    if (req.user) {
        Item.findById(req.params.itemid, function (err, item) {
            if (err)
                return next(err);

            if (item.interested.indexOf(req.user._id) != -1) {
                item.interested.remove(req.user._id);
                item.save((err2, doc) => {
                    if (err2)
                        return next(err2);

                    renderAccount(req, res, next, item.name + ' removed');
                });
            }
            else {
                renderAccount(req, res, next, item.name + ' already removed');
            }
        });
    }
    else {
        next(new Error('You are not logged in'));
    }
})

router.use((err, req, res, next) => {
    if (req.user) {
        console.log('err: ' + err);
        renderAccount(req, res, next, err.message);
    }
    else {
        next(err);
    }
});

module.exports = router;