const express = require('express');
const router = express.Router();
const Item = require('../models/Item');

router.get('/', (req, res, next) => {
    if (req.user && req.user.isAdmin) {
        Item.find({}, function (err, items) {
            if (err)
                return next(err);

            items.forEach(item => {
                item.interestedlength = item.interested.length;
            });

            res.render('admin',
                {
                    user: req.user,
                    items: items,
                    partials: {
                        header: '../views/partials/header',
                        footer: '../views/partials/footer',
                        msg: '../views/partials/msg'
                    }
                });
        });
    }
    else {
        res.redirect('/');
    }
});

router.post('/additem', (req, res, next) => {
    if (req.user && req.user.isAdmin) {
        Item.create(req.body, (err1, item) => {

            if (err1)
                return res.render('admin', {
                    user: req.user,
                    items: items,
                    message: err1.message,
                    partials: {
                        header: '../views/partials/header',
                        footer: '../views/partials/footer',
                        msg: '../views/partials/msg'
                    }
                });

            Item.find({}, function (err2, items) {
                if (err2)
                    return res.render('admin', {
                        user: req.user,
                        items: items,
                        message: err2.message,
                        partials: {
                            header: '../views/partials/header',
                            footer: '../views/partials/footer',
                            msg: '../views/partials/msg'
                        }
                    });


                items.forEach(item => {
                    item.interestedlength = item.interested.length;
                });

                res.render('admin', {
                    user: req.user,
                    items: items,
                    message: 'Item ' + item.name + ' created',
                    partials: {
                        header: '../views/partials/header',
                        footer: '../views/partials/footer',
                        msg: '../views/partials/msg'
                    }
                });
            });
        });
    }
    else {
        res.redirect('/');
    }
});

module.exports = router;