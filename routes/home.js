const express = require('express');
const router = express.Router();
const Item = require('../models/Item');
const User = require('../models/User');
const nodemailer = require('nodemailer');
const bcrypt = require('bcryptjs');

function renderHome(req, res, next, msg) {
    return renderHome(req, res, next, msg, null);
}
function renderHome(req, res, next, msg, link) {
    Item.aggregate(
        [
            {
                "$project": {
                    "name": 1,
                    "interested": 1,
                    "hits": { "$size": "$interested" }
                }
            },

            { "$match": { "hits": { "$gt": 0 } } },
            { "$sort": { "hits": -1 } },
            { "$limit": 5 }
        ],
        (err, items) => {
            if (err) {
                next(err);
            }
            else {
                items.forEach((item, i) => {
                    item.index = i + 1;
                });

                res.render('home', {
                    user: req.user,
                    top: items,
                    message: msg,
                    messageLink: link,
                    partials: {
                        header: '../views/partials/header',
                        footer: '../views/partials/footer',
                        msg: '../views/partials/msg'
                    }
                });
            }
        });
}

router.get('/', (req, res, next) => {
    return renderHome(req, res, next, null);
});

const randomString = (len) => {
    let text = '';
    const possible = "QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm1234567890";
    for (let i = 0; i < len; i++) {
        text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
}

Date.prototype.addHours = function (h) {
    this.setTime(this.getTime() + (h * 60 * 60 * 1000));
    return this;
}

router.post('/forgotpassword', (req, res, next) => {

    if (req.body.email.trim() == '')
        return next(new Error('Please enter the Email'));

    User.findOne({ "email": req.body.email }, function (err, user) {
        if (err)
            return next(err);

        if (!user)
            return next(new Error('Email not found'));

        user.nonce = randomString(10);
        user.passwordResetTime = new Date();
        user.save((err, doc) => {
            if (err)
                return next(err);

            nodemailer.createTestAccount((err, account) => {
                let transporter = nodemailer.createTransport({
                    host: account.smtp.host,
                    port: account.smtp.port,
                    secure: account.smtp.secure,
                    auth: {
                        user: account.user,
                        pass: account.pass
                    }
                });

                let info = transporter.sendMail({
                    from: '"sample store" <foo@example.com>',
                    to: user.email,
                    subject: 'Password reset',
                    text: 'Go to http://localhost:5000/forgotpassword/' + user.nonce + ' to reset your password. This link is valid for 2 hours.',
                    html: 'Go to <a href="http://localhost:5000/forgotpassword/' + user.nonce + '"> this page</a> to reset your password. This link is valid for 2 hours.'
                }, (err, info) => {
                    if (err)
                        return next(err);

                    console.log('forgot password: ' + req.body.email);
                    console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                    return renderHome(req, res, next, 'Reset password email sent to "' + req.body.email, nodemailer.getTestMessageUrl(info));
                });
            });
        });
    });
});

router.get('/forgotpassword/:nonce', (req, res, next) => {
    User.findOne({ "nonce": req.params.nonce }, (err, user) => {
        if (err || !user)
            return next(new Error('Invalid request'));


        if ((new Date() - user.passwordResetTime) > (2 * 60 * 60 * 1000)) {
            return next(new Error('This link has expired'));
        }

        res.render('resetPass', {
            user: user,
            partials: {
                header: '../views/partials/header',
                footer: '../views/partials/footer',
                msg: '../views/partials/msg'
            }
        });
    });
});

router.post('/resetpassword', (req, res, next) => {

    User.findOne({ "_id": req.body.user_id }, (err, user) => {

        if (err || !user)
            return next(new Error('Invalid request'));

        if ((req.body.password1.trim() == '') || (req.body.password2.trim() == '')) {
            return res.render('resetPass', {
                user: user,
                message: 'Please enter the password',
                partials: {
                    header: '../views/partials/header',
                    footer: '../views/partials/footer',
                    msg: '../views/partials/msg'
                }
            });
        }

        if (req.body.password1 != req.body.password2) {
            return res.render('resetPass', {
                user: user,
                message: 'The passwords do not match',
                partials: {
                    header: '../views/partials/header',
                    footer: '../views/partials/footer',
                    msg: '../views/partials/msg'
                }
            });
        }

        if ((new Date() - user.passwordResetTime) > (2 * 60 * 60 * 1000)) {
            return next(new Error('This link has expired'));
        }

        user.passwordResetTime = null;
        user.nonce = null;
        user.password = bcrypt.hashSync(req.body.password1, 10);;
        user.save((err, u) => {
            if (err)
                return next(err);

            return next(new Error('Password changed'));
        });
    });
});

router.use((err, req, res, next) => {
    console.log('err: ' + err);
    return renderHome(req, res, next, err.message);
});

module.exports = router;