const express = require('express');
const router = express.Router();
const passport = require('passport');

router.post('/', (req, res, next) => {
    passport.authenticate('localLogin', (error, user, info) => {

        if (req.body.email === '') {
            let e = new Error('Please enter the Email');
            e.body = req.body;
            return next(e);
        }
        if (req.body.password === '') {
            let e = new Error('Please enter the password');
            e.body = req.body;
            return next(e);
        }

        if (error) {
            return next(error);
        }

        req.logIn(user, (err) => {
            if (err) { return next(err); }
            return res.redirect('/account');
        });

    })(req, res, next);
});

module.exports = router;